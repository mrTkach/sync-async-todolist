import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import NewItemForm from "../components/NewItemForm";
import * as reduxHooks from "react-redux";
import * as actions from "../redux/slice";

jest.mock("react-redux");

const mockedDispatch = jest.spyOn(reduxHooks, "useDispatch");
const mockedRef = jest.spyOn(React, "useRef");

describe("NewItemForm", () => {
  it("onChange during typing works", () => {
    mockedRef.mockReturnValueOnce({ current: focus });

    render(<NewItemForm />);
    userEvent.type(screen.getByRole("todoInput"), "New item");
    expect(mockedRef).toBeCalledTimes(1);
  });

  it("NewItemForm snapshot", () => {
    const newItemForm = render(<NewItemForm />);
    expect(newItemForm).toMatchSnapshot();
  });

  // it("New item added", () => {
  //   const onClickHandler = jest.fn();
  //   mockedDispatch.mockReturnValue(onClickHandler);
  //   // const mockedToggleComplete = jest.spyOn(actions, "addTodo");
  //
  //   render(<NewItemForm />);
  //   userEvent.type(screen.getByRole("todoInput"), "New item");
  //
  //   fireEvent.click(screen.getByRole("button"));
  //
  //   // expect(dispatch).toHaveBeenCalledWith("New item");
  //   // expect(onClickHandler).toHaveBeenCalledTimes(1);
  // });
});
