import * as reduxHooks from "react-redux";
import { render } from "@testing-library/react";
import { Todo } from "types";
import TodoList from "components/TodoList";

jest.mock("react-redux");

describe("TodoList", () => {
  const todos: Todo[] = [
    {
      id: "123",
      userId: 1,
      title: "Test",
      completed: false,
    },
    {
      id: "124",
      userId: 1,
      title: "Redux",
      completed: false,
    },
  ];

  const useSelectorMock = jest.spyOn(reduxHooks, "useSelector");

  it("should create TodoList with empty todos", () => {
    useSelectorMock.mockReturnValue([]);
    const component = render(<TodoList />);
    expect(component).toMatchSnapshot();
  });

  it("should create TodoList with todo items", () => {
    useSelectorMock.mockReturnValue(todos);
    const component = render(<TodoList />);
    expect(component).toMatchSnapshot();
  });
});
