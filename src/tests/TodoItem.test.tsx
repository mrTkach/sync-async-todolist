import { fireEvent, render, screen } from "@testing-library/react";
import * as actions from "redux/slice";
import * as reduxHooks from "react-redux";
import TodoItem from "components/TodoItem";
import userEvent from "@testing-library/user-event";

const todoItemProps = {
  id: "123",
  userId: 1,
  title: "Just simple text",
  completed: false,
  toggleTodo: jest.fn(),
  removeTodo: jest.fn(),
};

describe("TodoItem", () => {
  it("should create TodoItem", () => {
    const component = render(<TodoItem {...todoItemProps} />);
    expect(component).toMatchSnapshot();
  });

  it("should dispatch actions", () => {
    render(<TodoItem {...todoItemProps} />);

    fireEvent.click(screen.getByRole("checkbox"));
    expect(todoItemProps.toggleTodo).toHaveBeenCalled();

    fireEvent.click(screen.getByRole("button"));
    expect(todoItemProps.removeTodo).toHaveBeenCalled();
  });
});
