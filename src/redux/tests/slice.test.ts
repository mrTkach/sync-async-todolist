import todoReducer, { addTodo, toggleTodo, removeTodo } from "../slice";

describe("todoSlice", () => {
  it("should return default state when passed an empty action", () => {
    // calling todoReducer and passing state as undefined and action with empty type {type: ""}
    const result = todoReducer(undefined, { type: "" });
    // todoReducer should return an empty array (initialState)
    expect(result).toEqual([]);
  });

  it("should add new todo item with 'assTodo' action", () => {
    const action = { type: addTodo.type, payload: "Redux" };
    const result = todoReducer([], action);

    // I expect that 'result' is an array and first element of result has title "Redux", which I passed in action.payload
    expect(result[0].title).toBe("Redux");
    // I expect that 'completed' field by default is false
    expect(result[0].completed).toBe(false);
  });

  it("should toggle todo completed status with 'toggleComplete' action", () => {
    const todos = [
      { id: "1234", userId: 12, title: "title", completed: false },
    ];
    const action = { type: toggleTodo.type, payload: "1234" };

    const result = todoReducer(todos, action);
    expect(result[0].completed).toBe(true);
  });

  it("should remove todo by id with 'removeTodo' action", () => {
    const todos = [
      { id: "1234", userId: 12, title: "title", completed: false },
    ];
    const action = { type: removeTodo.type, payload: "1234" };
    const result = todoReducer(todos, action);
    expect(result).toEqual([]);
  });
});
