import { selectAllTodos } from "../selectors";
import { Todo } from "../../types";

describe("redux selectors", () => {
  it("should select from state object", () => {
    const todos: Todo[] = [
      { id: "123", title: "todo title", userId: 1, completed: false },
    ];

    const result = selectAllTodos({ todos });
    expect(result).toEqual(todos);
  });
});
