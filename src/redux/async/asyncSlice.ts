import { createSlice } from "@reduxjs/toolkit";
import { Todo } from "../../types";
import {
  createSingleTodo,
  fetchAllTodos,
  removeSingleTodo,
  toggleSingleTodo,
} from "./asyncActions";

export type TodoSlice = {
  status: "idle" | "loading" | "finished" | "error";
  list: Todo[];
  error: null;
};

const initialState: TodoSlice = {
  status: "idle",
  list: [],
  error: null,
};

const asyncSlice = createSlice({
  name: "@todos",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchAllTodos.pending, (state) => {
        state.status = "loading";
        state.error = null;
      })
      .addCase(fetchAllTodos.fulfilled, (state, action) => {
        state.status = "finished";
        state.list = action.payload;
      })
      .addCase(fetchAllTodos.rejected, (state) => {
        state.status = "error";
      })
      .addCase(createSingleTodo.fulfilled, (state, action) => {
        state.list.unshift(action.payload);
      })
      .addCase(toggleSingleTodo.fulfilled, (state, action) => {
        const todo = state.list.find((el) => el.id === action.payload.id);
        if (todo) {
          todo.completed = action.payload.completed;
        }
      })
      .addCase(removeSingleTodo.fulfilled, (state, action) => {
        state.list = state.list.filter((todo) => todo.id !== action.payload);
      });
  },
});

export default asyncSlice.reducer;
