import { createAsyncThunk } from "@reduxjs/toolkit";
import { Todo } from "types";
import { TodoSlice } from "./asyncSlice";

const BASE_URL = "https://jsonplaceholder.typicode.com/todos";

export const fetchAllTodos = createAsyncThunk<
  Todo[],
  undefined,
  { state: { asyncTodos: TodoSlice } }
>(
  "todos/fetchTodos",
  async (_) => {
    const response: Response = await fetch(BASE_URL + "?_limit=10");
    const todoList: Todo[] = await response.json();
    return todoList;
  },
  {
    // to have action called just once, even in dev mode
    condition: (_, { getState }) => {
      const { status } = getState().asyncTodos;
      if (status === "loading") {
        return false;
      }
    },
  }
);

export const createSingleTodo = createAsyncThunk<Todo, string>(
  "todo/createSingleTodo",
  async (text: string) => {
    const newTodo: Required<Omit<Todo, "id">> = {
      title: text,
      userId: 1,
      completed: false,
    };
    const response: Response = await fetch(BASE_URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newTodo),
    });
    return await response.json();
  }
);

export const toggleSingleTodo = createAsyncThunk<
  Todo,
  Todo["id"],
  { state: { asyncTodos: TodoSlice }; rejectValue: string }
>("todo/toggleTodo", async (id: string, { getState, rejectWithValue }) => {
  const todo = getState().asyncTodos.list.find((el) => el.id === id);
  if (todo) {
    const response = await fetch(BASE_URL + "/" + id, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        completed: !todo.completed,
      }),
    });
    if (!response.ok) {
      return rejectWithValue("Impossible to update todo with id " + id);
    }
    return await response.json();
  }

  return rejectWithValue("No such todo with id " + id);
});

export const removeSingleTodo = createAsyncThunk<
  Todo["id"],
  Todo["id"],
  { rejectValue: string }
>("todo/removeTodo", async (id: Todo["id"], { rejectWithValue }) => {
  const response = await fetch(BASE_URL + "/" + id, {
    method: "DELETE",
  });

  if (!response.ok) {
    return rejectWithValue("Impossible to delete todo with id " + id);
  }

  return id;
});
