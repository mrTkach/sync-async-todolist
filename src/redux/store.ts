import { combineReducers, configureStore } from "@reduxjs/toolkit";
import todoReducer from "./slice";
import asyncTodoReducer from "./async/asyncSlice";

const rootReducer = combineReducers({
  todos: todoReducer,
  asyncTodos: asyncTodoReducer,
});

export const store = configureStore({
  reducer: rootReducer,
});

// export type RootState = ReturnType<typeof rootReducer>; or =>
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
