import { Todo } from "../types";

export const selectAllTodos = (state: { todos: Todo[] }) => state.todos;
