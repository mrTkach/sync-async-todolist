import NewItemForm from "components/NewItemForm";
import TodoList from "components/TodoList";
import AsyncTodoList from "components/AsyncTodoList";
import AsyncNewItemForm from "components/AsyncNewItemForm";
import "./App.css";

const App = () => (
  <div className="App">
    <NewItemForm />
    <TodoList />

    <hr />

    <AsyncNewItemForm />
    <AsyncTodoList />
  </div>
);

export default App;
