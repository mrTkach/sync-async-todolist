import { useRef, KeyboardEvent } from "react";
import { useAppDispatch } from "redux/redux-hooks";
import { createSingleTodo } from "redux/async/asyncActions";

const AsyncNewItemForm = () => {
  const dispatch = useAppDispatch();
  const inputRef = useRef<HTMLInputElement | null>(null);

  const clickHandler = () => {
    if (!inputRef.current?.value.trim()) return;
    if (inputRef.current) {
      dispatch(createSingleTodo(inputRef.current.value.trim()));
      inputRef.current.value = "";
    }
  };

  const onEnterHandler = (e: KeyboardEvent<HTMLInputElement>) => {
    e.code === "Enter" && clickHandler();
  };

  return (
    <>
      <input
        type="text"
        placeholder="Type in your task"
        ref={inputRef}
        onKeyDown={onEnterHandler}
      />
      <button onClick={clickHandler}>Add todo</button>
    </>
  );
};

export default AsyncNewItemForm;
