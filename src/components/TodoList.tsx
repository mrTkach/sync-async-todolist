import { Todo } from "types";
import TodoItem from "components/TodoItem";
import { useAppDispatch, useAppSelector } from "redux/redux-hooks";
import { selectAllTodos } from "redux/selectors";
import { removeTodo, toggleTodo } from "redux/slice";

const TodoList = () => {
  const todoList = useAppSelector(selectAllTodos);
  const dispatch = useAppDispatch();

  const handleRemoveTodo = (id: Todo["id"]) => {
    dispatch(removeTodo(id));
  };

  const handleToggleTodo = (id: Todo["id"]) => {
    dispatch(toggleTodo(id));
  };

  return (
    <ul>
      {todoList.map((todo: Todo) => (
        <TodoItem
          key={todo.id}
          {...todo}
          removeTodo={handleRemoveTodo}
          toggleTodo={handleToggleTodo}
        />
      ))}
    </ul>
  );
};

export default TodoList;
