import { useEffect } from "react";
import { Todo } from "types";
import { useAppDispatch, useAppSelector } from "redux/redux-hooks";
import { selectAsyncTodos } from "redux/async/asyncSelectors";
import {
  fetchAllTodos,
  removeSingleTodo,
  toggleSingleTodo,
} from "redux/async/asyncActions";
import TodoItem from "components/TodoItem";

const AsyncTodoList = () => {
  const { list } = useAppSelector(selectAsyncTodos);
  const dispatch = useAppDispatch();

  const handleRemoveTodo = (id: Todo["id"]) => {
    dispatch(removeSingleTodo(id));
  };

  const handleToggleTodo = (id: Todo["id"]) => {
    dispatch(toggleSingleTodo(id));
  };

  useEffect(() => {
    dispatch(fetchAllTodos());
  }, []);

  return (
    <ul>
      {list.map((todo: Todo) => (
        <TodoItem
          key={todo.id}
          {...todo}
          removeTodo={handleRemoveTodo}
          toggleTodo={handleToggleTodo}
        />
      ))}
    </ul>
  );
};

export default AsyncTodoList;
