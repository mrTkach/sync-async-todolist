import { useRef, KeyboardEvent } from "react";
import { useAppDispatch } from "redux/redux-hooks";
import { addTodo } from "redux/slice";

const NewItemForm = () => {
  const dispatch = useAppDispatch();
  const inputRef = useRef<HTMLInputElement | null>(null);

  const clickHandler = () => {
    if (!inputRef.current?.value.trim()) return;
    if (inputRef.current) {
      dispatch(addTodo(inputRef.current.value.trim()));
      inputRef.current.value = "";
    }
  };

  const onEnterHandler = (e: KeyboardEvent<HTMLInputElement>) => {
    e.code === "Enter" && clickHandler();
  };

  return (
    <>
      <input
        type="text"
        role="todoInput"
        placeholder="Type in your task"
        ref={inputRef}
        onKeyDown={onEnterHandler}
      />
      <button onClick={clickHandler} role="button">
        Add todo
      </button>
    </>
  );
};

export default NewItemForm;
